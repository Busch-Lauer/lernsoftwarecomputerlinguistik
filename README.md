**Lehrprogramme für Deutsch als Fremdsprache** 

Recherchieren Sie, welche Sprachlernsoftware für Deutsch als Fremdsprache derzeit
im Markt verfügbar ist.
Erstellen Sie eine Übersicht und kategorisieren sie die Software nach verfügbaren
Funktionalitäten, nach Fertigkeitsentwicklung (Hören, Sprechen, Schreiben, Lesen),
nach Sprachniveaustufen und weiteren Nutzerkriterien (Benutzeroberfläche,
Bedienfreundlichkeit, Lernerfolg usw.).
Nachdem Sie einige Programme getestet haben, bewerten Sie diese Auswahl aus
der Perspektive des Nutzers, stellen Vor- und Nachteile heraus sowie Defizite, die es
für die Verbesserung von Funktionalität und Nutzererlebnis aus Ihrer Sicht zu
beachten gibt.
Hinweis: Es empfiehlt sich sicher auch eine Diskussion mit anderen Mitstudenten,
deren erste Sprache nicht Deutsch ist. Wenn Sie zu einer Einschätzung gekommen
sind, welche Programme gut und welche weniger gut geeignet sind, begründen Sie
auch, wie Sie zu dieser Einschätzung kommen. Daraus soll abgeleitet werden, was
bei der Erstellung von Lernsoftware allgemein und Sprachlern-Software im
Besonderen zu beachten ist. Suchen Sie zu diesem Thema auch Belege in der
wissenschaftlichen Literatur.


